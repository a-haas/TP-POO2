class Vehicule
    attr_reader :estCamion
end

class Voiture < Vehicule
    def initialize()
        @estCamion = false
    end
end

class Camion < Vehicule
    attr_reader :nbreEssieu, :poidsTotal

    def initialize(nbreEssieu, poidsTotal)
        @estCamion = true
        @nbreEssieu = nbreEssieu
        @poidsTotal = poidsTotal
    end
end

class Peage
    attr_reader :nbVehicule, :total

    def initialize
        @nbVehicule = 0
        @total = 0
    end

    def passage(vehicule)
        if vehicule.estCamion
            @total += 7 * vehicule.nbreEssieu
            @total += 15 * vehicule.poidsTotal
        else
            @total += 4
        end
        @nbVehicule += 1
        puts @nbVehicule
        puts @total
    end
end

p = Peage.new
c = Camion.new(2, 1)
v = Voiture.new

p.passage(v)
p.passage(c)
p.passage(v)