#!/usr/bin/ruby -w

class BinomialExpression
    protected
    attr_reader :a, :b, :c, :dis

    public
    def initialize(a, b, c, dis)
        @a = a
        @b = b
        @c = c
        @dis = dis
    end

    public
    def self.create(a, b, c)
        delta = (b * b) - (4.0 * a * c)
        if delta < 0
            return BinomialExpression0.new(a, b, c, delta)
        elsif delta == 0
            return BinomialExpression1.new(a, b, c, delta)
        else
            return BinomialExpression2.new(a, b, c, delta)
        end
    end

    def compute_root()
        puts 'Error'
    end

    def numberOfRoots()
        puts 'Error'
    end

    def rootValue(i)
        puts 'Error'
    end
end

class BinomialExpression0 < BinomialExpression

    def numberOfRoots()
        return 0
    end

    def rootValue(i)
        puts 'No roots for this expression'
    end
end

class BinomialExpression1 < BinomialExpression

    attr_accessor :sol

    def initialize(a, b, c, dis)
        super(a,b,c,dis)
        self.compute_root()
    end

    def compute_root()
        @sol = - (@b / 2*@a)
    end

    def numberOfRoots()
        return 1
    end

    def rootValue(i)
        if i != 1
            raise 'Error'
            return
        end

        return @sol
    end
end

class BinomialExpression2 < BinomialExpression

    attr_accessor :sol1, :sol2

    def initialize(a, b, c, dis)
        super(a,b,c,dis)
        self.compute_root()
    end

    def compute_root()
        @sol1 = (-@b - Math.sqrt(dis)) / 2*@a
        @sol2 = (-@b + Math.sqrt(dis)) / 2*@a
    end

    def numberOfRoots()
        return 2
    end

    def rootValue(i)
        if i == 1
            puts @sol1
        elsif i == 2
            puts @sol2
        else
            puts "Error"
        end
        return
    end
end

(BinomialExpression.create(1,1,1)).rootValue(1)

(BinomialExpression.create(1,2,1)).rootValue(1)

(BinomialExpression.create(1,3,1)).rootValue(1)
(BinomialExpression.create(1,3,1)).rootValue(2)