class Animal
    attr_reader :name

    def initialize(name = "")
        @name = name
    end

    def getType()
        return "Je suis un animal " + @name
    end
end

class Mammifere < Animal
    def getType()
        return super() + " Je suis un mammifère"
    end
end

animaux = []
animaux.append(Animal.new("truc"))
animaux.append(Animal.new())
animaux.append(Mammifere.new())

for a in animaux
    puts a.getType()
ends