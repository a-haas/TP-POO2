﻿abstract class Suite
{
    public int u1 { get; }
    public int pas { get; }

    public Suite(int u1, int pas)
    {
        this.u1 = u1;
        this.pas = pas;
    }
    
    public abstract int valeurAuRangN(int rang);
    public abstract int sommeAuRangN(int rang);
}

class SuiteArithm : Suite
{
    public SuiteArithm(int u1, int pas) : base(u1, pas) {}
    public override int valeurAuRangN(int rang)
    {
        return this.u1 + (rang - 1) * this.pas;
    }

    public override int sommeAuRangN(int rang)
    {
        return (rang / 2) * (this.u1 + this.valeurAuRangN(rang));
    }
}

class SuiteGeom : Suite
{
    public SuiteGeom(int u1, int pas) : base(u1, pas) {}
    public override int valeurAuRangN(int rang)
    {
        return this.u1 * (int) Math.Pow(pas, rang - 1);
    }

    public override int sommeAuRangN(int rang)
    {
        return this.u1 * (1 - (int) Math.Pow(this.pas, rang)) / (1 - this.pas);
    }
}

class Test
{
    public static void Main(string[] args)
    {
        SuiteArithm s1=new SuiteArithm(0,3);
        SuiteGeom s2=new SuiteGeom(2,2);

        Console.WriteLine(
            "Suite Arithmétique U(n+1)=Un+" + s1.pas
            + " avec U1=" + s1.u1
            + "\nValeur au rang n=4: " + s1.valeurAuRangN(4) 
            + "\nSomme au rang n=4: " +s1.sommeAuRangN(4));
        
        Console.WriteLine(
            "Suite Géométrique U(n+1)=Un*" +s2.pas 
            +" avec U1=" +s2.u1
            +":\nValeur au rang n=2: " +s2.valeurAuRangN(2)
            +"\nSomme au rang n=2: " +s2.sommeAuRangN(2));
    }
}