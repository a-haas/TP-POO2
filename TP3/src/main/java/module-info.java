module com.poo2.tp3 {
    requires javafx.controls;
    requires javafx.fxml;
    requires kotlin.stdlib;


    opens com.poo2.tp3 to javafx.fxml;
    exports com.poo2.tp3;
}