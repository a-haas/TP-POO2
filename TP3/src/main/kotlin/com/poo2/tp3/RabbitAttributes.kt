package com.poo2.tp3

enum class RabbitAttributes {
    ID, AGE, WEIGHT, HEIGHT
}