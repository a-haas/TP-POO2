package com.poo2.tp3

class RabbitFarm(val rabbits: MutableList<Rabbit>) {

    fun addRabbit(r: Rabbit) {
        rabbits.add(r)
    }

    fun deleteRabbit(r: Rabbit) {
        rabbits.remove(r)
    }

    fun deleteRabbit(id: Int) {
        deleteRabbit(
            rabbits.firstNotNullOf { item -> item.takeIf { it.id == id } }
        )
    }

    fun generateRabbits(amount: Int) {
        for (i in 1..amount) {
            val age = (Math.random() * (15 - 1 + 1)).toInt() + 1
            val weight = (Math.random() * (20.0 - 0.2 + 1)) + 0.2
            val height = (Math.random() * (45.0 - 5.0 + 1)) + 5.0
            addRabbit(Rabbit(age, weight, height))
        }
    }

    fun getRabbits(type: RabbitAttributes, ascending: Boolean): List<Rabbit> {
        return when (type) {
            RabbitAttributes.ID -> {
                if (ascending) rabbits.sortedBy { it.id } else rabbits.sortedByDescending { it.id }
            }
            RabbitAttributes.AGE -> {
                if (ascending) rabbits.sortedBy { it.age } else rabbits.sortedByDescending { it.age }
            }
            RabbitAttributes.WEIGHT -> {
                if (ascending) rabbits.sortedBy { it.weight } else rabbits.sortedByDescending { it.weight }
            }
            RabbitAttributes.HEIGHT -> {
                if (ascending) rabbits.sortedBy { it.height } else rabbits.sortedByDescending { it.height }
            }
        }
    }

}