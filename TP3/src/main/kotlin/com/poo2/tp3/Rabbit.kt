package com.poo2.tp3

import javafx.scene.text.FontWeight

class Rabbit {
    companion object {
        private var nextID = 1
    }
    val id: Int
    val age: Int
    val weight: Double
    val height: Double

    constructor(age: Int, weight: Double, height: Double) {
        this.id = Rabbit.nextID
        this.age = age
        this.weight = weight
        this.height = height

        Rabbit.nextID += 1
    }
}